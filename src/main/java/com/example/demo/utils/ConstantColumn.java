/////////////////////////////////////////////////////////////////////////////
//
// � 2021 IDTU-CS3332IRFA-21TSP
//
/////////////////////////////////////////////////////////////////////////////

package com.example.demo.utils;

/**
 * [OVERVIEW] Constant Column.
 *
 * @author: LinhDT
 * @version: 1.0
 * @History
 * [NUMBER]  [VER]     [DATE]          [USER]             [CONTENT]
 * --------------------------------------------------------------------------
 * 001       1.0       2021/04/08      LinhDT       	  Create new
*/
public class ConstantColumn {

    public final static String PHONE = "phone";
    public final static String USERNAME = "username";
    public final static String PASSWORD = "password";
    public final static String EMAIL = "email";
    public final static String DOB = "dob";
    public final static String ADDRESS = "address";
    public final static String ROLE = "role";

    public final static String CONFIRMED_PASSWORD = "confirmed_password";
    public final static String CURRENT_PASSWORD = "current_password";

    public final static String BOOK_ID = "book_id";
    public final static String BOOK_NAME = "book_name";
    public final static String AUTHOR = "author";
    public final static String CATEGORY = "category";
    public final static String CATEGORY_NAME = "category_name";
    public final static String PUBLICATION_DATE = "publication_date";
    
    public final static String BARCODE = "barcode";
}
